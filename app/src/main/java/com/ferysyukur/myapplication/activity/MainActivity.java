package com.ferysyukur.myapplication.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ferysyukur.myapplication.R;


public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);
        
        TextView titleMovie = findViewById(R.id.titleMovie);
//        TextView titleMovie2 = findViewById(R.id.titleMovie2);
        LinearLayout itemMovie1 = findViewById(R.id.itemMovie1);
//        LinearLayout itemMovie2 = findViewById(R.id.itemMovie2);
        
        titleMovie.setText("Thor: Ragnarok");
//        titleMovie2.setText(titleMovie.getText());

        itemMovie1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, MovieDetailActivity.class);
                startActivity(intent);
                Log.i(TAG, "onClick: Tertekan deh");
            }
        });

//        itemMovie2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Toast.makeText(MainActivity.this, "Klik Item Movie 2", Toast.LENGTH_SHORT).show();
//            }
//        });
    }
}
