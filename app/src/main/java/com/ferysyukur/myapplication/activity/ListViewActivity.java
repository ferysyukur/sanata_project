package com.ferysyukur.myapplication.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.ferysyukur.myapplication.R;
import com.ferysyukur.myapplication.model.MovieItem;

import java.util.ArrayList;

public class ListViewActivity extends AppCompatActivity {

    private static final String TAG = ListViewActivity.class.getName();
    public static final String EXTRA_TITLE = "extra_title";
    private static final String EXTRA_TITLE2 = "title2";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);

        ListView movieList = findViewById(R.id.listView);

        final ArrayList<MovieItem> data = new ArrayList<>();

        for (int i=0; i<10; i++){
            MovieItem movieItem = new MovieItem();
            movieItem.setTitle("The Movie#"+String.valueOf(i));
            movieItem.setOverview("Smash"+String.valueOf(i));
            movieItem.setVoteAverage(i);

            String posterId = String.valueOf(i + 1);
            movieItem.setPosterPath("http://lorempixel.com/150/250/sports/" + posterId);

            data.add(movieItem);
        }

        MovieAdapter adapter = new MovieAdapter(this, R.layout.list_view_item, data);

        movieList.setAdapter(adapter);

        movieList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                MovieItem movieItem = data.get(i);
                Toast.makeText(ListViewActivity.this, "Test", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(ListViewActivity.this, MovieDetailActivity.class);
                intent.putExtra(EXTRA_TITLE, movieItem.getTitle());
                // todo : lengkapi field lainnya
                startActivity(intent);
            }
        });

    }
}
