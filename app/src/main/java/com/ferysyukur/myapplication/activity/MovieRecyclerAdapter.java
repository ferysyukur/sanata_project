package com.ferysyukur.myapplication.activity;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ferysyukur.myapplication.R;
import com.ferysyukur.myapplication.model.MovieItem;

import java.util.ArrayList;
import java.util.List;

public class MovieRecyclerAdapter extends RecyclerView.Adapter<MovieRecyclerAdapter.MovieViewHolder> {

    List<MovieItem> listMovie = new ArrayList<>();
    private MovieClickListener movieClickListener;

    public interface MovieClickListener {
        void onMovieClick(MovieItem movieItem);
    }

    @Override
    public MovieViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_view_item, parent, false);
        return new MovieViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MovieViewHolder holder, final int position) {
        final MovieItem movie = listMovie.get(position);
        holder.bindMovie(movie);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (movieClickListener != null) {
                    movieClickListener.onMovieClick(movie);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return listMovie.size();
    }

    public void setMovieClickListener(MovieClickListener listener) {
        this.movieClickListener = listener;
    }

    public void addMovie(List<MovieItem> movies) {
        this.listMovie = movies;
        notifyDataSetChanged();
    }

    public void clearMovie() {
        this.listMovie.clear();
        notifyDataSetChanged();
    }

    public static class MovieViewHolder extends RecyclerView.ViewHolder {

        private final TextView titleMovie;
        private final ImageView ivPoster;
        private final TextView overviewMovie;
        private final TextView voteAverageMovie;

        public MovieViewHolder(View itemView) {
            super(itemView);
            // find view by id
            ivPoster = itemView.findViewById(R.id.posterMovie);
            titleMovie = itemView.findViewById(R.id.titleMovie);
            overviewMovie = itemView.findViewById(R.id.overviewMovie);
            voteAverageMovie = itemView.findViewById(R.id.ratingMovie);
        }

        public void bindMovie(MovieItem movieItem) {
            // set view berdasarkan data movie
            titleMovie.setText(movieItem.getTitle());
            overviewMovie.setText(movieItem.getOverview());
            voteAverageMovie.setText(String.valueOf(movieItem.getVoteAverage()));

            Glide.with(itemView.getContext())
                    .load(movieItem.getPosterPath())
                    .into(ivPoster);
        }

    }

}
