package com.ferysyukur.myapplication.activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.ferysyukur.myapplication.R;
import com.ferysyukur.myapplication.database.DbService;
import com.ferysyukur.myapplication.model.MovieItem;

import java.util.IllegalFormatCodePointException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.ferysyukur.myapplication.activity.RecyclerViewActivity.EXTRA_MODEL;

public class MovieDetailActivity extends AppCompatActivity {

    @BindView(R.id.backdropImg)
    ImageView backdropImg;
    @BindView(R.id.posterImg)
    ImageView posterImg;
    @BindView(R.id.titleTxt)
    TextView titleTxt;
    @BindView(R.id.taglineTxt)
    TextView taglineTxt;
    @BindView(R.id.statusTxt)
    TextView statusTxt;
    @BindView(R.id.homepageTxt)
    TextView homepageTxt;
    @BindView(R.id.overviewTxt)
    TextView overviewTxt;
    @BindView(R.id.overviewDesc)
    TextView overviewDesc;
    @BindView(R.id.tvRating)
    TextView tvRating;
    @BindView(R.id.fabSave)
    FloatingActionButton fabSave;

    private DbService dbService;
    private MovieItem movieItem;
    private boolean isSaved;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);
        ButterKnife.bind(this);

        dbService = new DbService(this);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);

        movieItem = getIntent().getParcelableExtra(EXTRA_MODEL);
        checkIsMovieSaved(movieItem.getId());

        titleTxt.setText(movieItem.getTitle());
        overviewDesc.setText(movieItem.getOverview());
        String voteValue = String.valueOf(movieItem.getVoteAverage());
        tvRating.setText("Rating " + voteValue + " stars");

        Glide.with(this)
                .load(movieItem.getPosterPath())
                .into(posterImg);

        Glide.with(this)
                .load(movieItem.getBackdropPath())
                .into(backdropImg);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.titleTxt)
    public void onViewClicked() {
        //
    }

    @OnClick(R.id.fabSave)
    public void saveMovie() {
        if (isSaved) {
            // remove
            boolean isRemoveSuccess = dbService.removeMovie(movieItem.getId());
            if (isRemoveSuccess) {
                isSaved = false;
                fabSave.setImageResource(R.drawable.ic_favorite_border_black_24dp);
            }
        } else {
            // insert
            boolean isSaveSuccess = dbService.saveMovie(movieItem);
            String message = isSaveSuccess ? "Berhasil mengimpan movie" : "Movie gagal disimpan";
            if (isSaveSuccess) {
                isSaved = true;
                fabSave.setImageResource(R.drawable.ic_favorite_black_24dp);
            }
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
    }

    private void checkIsMovieSaved(int movieId) {
        isSaved = dbService.isMovieSaved(movieId);

        if (isSaved) {
            fabSave.setImageResource(R.drawable.ic_favorite_black_24dp);
        } else {
            fabSave.setImageResource(R.drawable.ic_favorite_border_black_24dp);
        }

    }

}
