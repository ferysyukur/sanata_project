package com.ferysyukur.myapplication.activity;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ferysyukur.myapplication.R;
import com.ferysyukur.myapplication.model.MovieItem;

import java.util.List;

/**
 * Created by ferysyukur on 09/11/17.
 */

public class MovieAdapter extends ArrayAdapter<MovieItem> {

    Context context;
    int resource;
    List<MovieItem> movieItemList;

    public MovieAdapter(@NonNull Context context, int resource, @NonNull List<MovieItem> objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
        movieItemList = objects;
    }

    static class MovieHolder{
        ImageView posterImg;
        TextView titleMovie, overviewMovie, voteAverageMovie;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View row = convertView;
        MovieHolder holder = null;

        if(row == null){
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(resource, parent, false);

            holder = new MovieHolder();
            holder.posterImg = row.findViewById(R.id.posterMovie);
            holder.titleMovie = row.findViewById(R.id.titleMovie);
            holder.overviewMovie = row.findViewById(R.id.overviewMovie);
            holder.voteAverageMovie = row.findViewById(R.id.ratingMovie);

            row.setTag(holder);
        }else{
            holder = (MovieHolder) row.getTag();
        }

        MovieItem movieItem = movieItemList.get(position);
        holder.titleMovie.setText(movieItem.getTitle());
        holder.overviewMovie.setText(movieItem.getOverview());
        holder.voteAverageMovie.setText(String.valueOf(movieItem.getVoteAverage()));

        Glide.with(context)
                .load(movieItem.getPosterPath())
                .into(holder.posterImg);

        return row;
    }
}
