package com.ferysyukur.myapplication.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.ferysyukur.myapplication.R;
import com.ferysyukur.myapplication.database.DbService;
import com.ferysyukur.myapplication.model.ApiResponse;
import com.ferysyukur.myapplication.model.MovieItem;
import com.ferysyukur.myapplication.service.ApiClient;
import com.ferysyukur.myapplication.service.MovieService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecyclerViewActivity extends AppCompatActivity {

    public static final String EXTRA_MODEL = "movieItem";
    private static final String TAG = RecyclerViewActivity.class.getSimpleName();
    private RecyclerView recyclerView;
    private MovieRecyclerAdapter adapter;
    private static final String EXTRA_TITLE = "extra_title";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view);

        recyclerView = findViewById(R.id.recyclerView);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new MovieRecyclerAdapter();
        recyclerView.setAdapter(adapter);

        adapter.setMovieClickListener(new MovieRecyclerAdapter.MovieClickListener() {
            @Override
            public void onMovieClick(MovieItem movieItem) {
                Intent intent = new Intent(RecyclerViewActivity.this, MovieDetailActivity.class);
                intent.putExtra(EXTRA_MODEL, movieItem);
                // todo : lengkapi field lainnya
                startActivity(intent);
            }
        });

        //populateDummyMovie();
        getPopularMovie();
        getFavoriteMovie();
    }

    private void populateDummyMovie() {
        List<MovieItem> movies = new ArrayList<>();

        for (int i=0; i<10; i++){
            MovieItem movieItem = new MovieItem();
            movieItem.setTitle("The Movie#"+ String.valueOf(i));
            movieItem.setOverview("Smash "+ String.valueOf(i));
            movieItem.setVoteAverage(i);

            String posterId = String.valueOf(i + 1);
            movieItem.setPosterPath("http://lorempixel.com/150/250/sports/" + posterId);

            movies.add(movieItem);
        }

        adapter.addMovie(movies);
    }

    private void getPopularMovie() {

        MovieService movieService = ApiClient.getMovieService();
        movieService.getPopularMovies("a89d01272e751aeb5908ace347c3ad46", "1")
                .enqueue(new Callback<ApiResponse>() {
                    @Override
                    public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
                        if (response.isSuccessful()) {
                            ApiResponse apiResponse = response.body();
                            List<MovieItem> movies = apiResponse.getResults();
                            adapter.addMovie(movies);
                        }
                    }

                    @Override
                    public void onFailure(Call<ApiResponse> call, Throwable t) {
                        Log.e(TAG, "onFailure: ", t);
                    }
                });

    }

    private void getFavoriteMovie() {
        DbService dbService = new DbService(this);
        List<MovieItem> movies = dbService.getFavoriteMovies();
        for (MovieItem movieItem : movies) {
            Log.d(TAG, "favorite movie -> " + movieItem.getTitle());
        }
    }



}
