package com.ferysyukur.myapplication.service;

import com.ferysyukur.myapplication.model.ApiResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface MovieService {

    @GET("movie/popular")
    Call<ApiResponse> getPopularMovies(
            @Query("api_key") String apiKey,
            @Query("page") String page
    );

}
