package com.ferysyukur.myapplication.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.ferysyukur.myapplication.database.DbContract.MovieContract;
import com.ferysyukur.myapplication.model.MovieItem;

import java.util.ArrayList;
import java.util.List;

public class DbService extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "movie_db";
    private static final int DATABASE_VERSION = 1;

    private static final String SQL_CREATE_TABLE_MOVIE =
            "CREATE TABLE " + MovieContract.TABLE_NAME + " (" +
                    MovieContract.ID_MOVIE + " INTEGER PRIMARY KEY, " +
                    MovieContract.TITLE + " TEXT, " +
                    MovieContract.OVERVIEW + " TEXT, " +
                    MovieContract.IMG_BACKDROP + " TEXT, " +
                    MovieContract.IMG_POSTER + " TEXT, " +
                    MovieContract.VIDEO + " INTEGER, " +
                    MovieContract.VOTE_AVERAGE + " DOUBLE" +
            ");";

    private static final String SQL_DROP_TABLE_MOVIE = "DROP TABLE IF EXISTS " + MovieContract.TABLE_NAME;

    public DbService(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // create table
        db.beginTransaction();
        try {
            db.execSQL(SQL_CREATE_TABLE_MOVIE);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            Log.d("DbService", e.getMessage());
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DROP_TABLE_MOVIE);
        onCreate(db);
    }

    public boolean saveMovie(MovieItem movieItem) {

        // membuka koneksi database
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put(MovieContract.ID_MOVIE, movieItem.getId());
        cv.put(MovieContract.TITLE, movieItem.getTitle());
        cv.put(MovieContract.OVERVIEW, movieItem.getOverview());
        cv.put(MovieContract.IMG_BACKDROP, movieItem.getBackdropPath());
        cv.put(MovieContract.IMG_POSTER, movieItem.getPosterPath());
        cv.put(MovieContract.VIDEO, movieItem.isVideo() ? 1 : 0);
        cv.put(MovieContract.VOTE_AVERAGE, movieItem.getVoteAverage());

        // menyimpan data
        // jika row id > 0 maka insert berhasil
        long rowId = db.insert(MovieContract.TABLE_NAME, null, cv);

        //tutup koneksi database
        db.close();

        return rowId > 0;
    }

    public boolean isMovieSaved(int movieId) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(
                MovieContract.TABLE_NAME,
                null /* select semua kolom */,
                MovieContract.ID_MOVIE + "=?",
                new String[] { String.valueOf(movieId)},
                null,
                null,
                null
        );


        int totalRow = cursor.getCount();
        cursor.close();
        db.close();

        return totalRow > 0;
    }

    public List<MovieItem> getFavoriteMovies() {
        SQLiteDatabase db = this.getReadableDatabase();

        // select * from movie
        Cursor cursor = db.query(
                MovieContract.TABLE_NAME,
                null,
                null,
                null,
                null,
                null,
                null
        );

        int results = cursor.getCount();
        List<MovieItem> movies = new ArrayList<>();
        if (results > 0) {
            cursor.moveToFirst();
            do {
                int id = cursor.getInt(cursor.getColumnIndex(MovieContract.ID_MOVIE));
                String title = cursor.getString(cursor.getColumnIndex(MovieContract.TITLE));
                String overview = cursor.getString(cursor.getColumnIndex(MovieContract.OVERVIEW));
                String backdrop = cursor.getString(cursor.getColumnIndex(MovieContract.IMG_BACKDROP));
                String poster = cursor.getString(cursor.getColumnIndex(MovieContract.IMG_POSTER));
                double vote = cursor.getDouble(cursor.getColumnIndex(MovieContract.VOTE_AVERAGE));
                int video = cursor.getInt(cursor.getColumnIndex(MovieContract.VIDEO));
                // todo : baca kolom lainnya

                MovieItem item = new MovieItem();
                item.setId(id);
                item.setTitle(title);
                item.setOverview(overview);
                item.setBackdropPath(backdrop);
                item.setPosterPath(poster);
                item.setVideo(video == 1);
                item.setVoteAverage(vote);

                movies.add(item);

            } while (cursor.moveToNext());

        }

        cursor.close();
        db.close();

        return movies;

    }

    public boolean removeMovie(int movieId) {
        SQLiteDatabase db = getReadableDatabase();
        int rowEffected = db.delete(
                MovieContract.TABLE_NAME,
                MovieContract.ID_MOVIE + "=?",
                new String[] {String.valueOf(movieId)}
        );

        db.close();

        return rowEffected > 0;
    }

}
