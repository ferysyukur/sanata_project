package com.ferysyukur.myapplication.database;

public class DbContract {

    public static final class MovieContract {
        public static final String TABLE_NAME = "movie";

        public static final String ID_MOVIE = "id_movie";
        public static final String TITLE = "title";
        public static final String OVERVIEW = "overview";
        public static final String IMG_BACKDROP = "img_backdrop";
        public static final String IMG_POSTER = "img_poster";
        public static final String VIDEO = "video";
        public static final String VOTE_AVERAGE = "vote_average";
    }

}
